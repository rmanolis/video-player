package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func router(r *gin.Engine) {
	hub := newHub()
	go hub.run()

	r.GET("/ws", func(c *gin.Context) {
		serveWs(hub, c.Writer, c.Request)
	})

	r.Static("/assets", "./assets")
	r.StaticFile("/", "./assets/media-player.html")
	//r.Static("/video.mp4", "/home/manos/Downloads/shot-caller.mp4")
	r.GET("/video.mp4", func(c *gin.Context) {
		http.ServeFile(c.Writer, c.Request, "/home/manos/Downloads/shot-caller.mp4")
	})
}
